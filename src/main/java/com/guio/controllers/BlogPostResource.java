package com.guio.controllers;

import com.guio.models.Blogs.BlogPost;
import io.quarkus.runtime.StartupEvent;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.jboss.resteasy.reactive.RestQuery;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Produces(MediaType.APPLICATION_JSON)
@Path("/blogpost")
public class BlogPostResource {

    @Inject
    SearchSession searchSession;

    /**
     * When the application starts, if there are any blog posts in the database, then index them.
     *
     * @param ev The event that is being observed.
     */
    @Transactional
    public void onStartup(@Observes StartupEvent ev) throws InterruptedException {
        if(BlogPost.count() > 0) {
            searchSession.massIndexer().startAndWait();
        }
    }

    /**
     * "Search for blog posts that match the given pattern, and return the first 20 results, sorted by title, body, and
     * description."
     * <p>
     * The first line of the function is the @GET annotation, which tells the server that this function should be called
     * when a GET request is made to the URL /Blogpost/search
     *
     * @param pattern The search pattern to use.
     * @param size The number of results to return.
     * @return A list of BlogPost objects
     */
    @GET
    @Path("Blogpost/search")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public List<BlogPost> searchBlogs(@RestQuery String pattern,
                                        @RestQuery Optional<Integer> size) {
        return searchSession.search(BlogPost.class)
                .where(f -> pattern == null || pattern.trim().isEmpty() ? f.matchAll()
                        : f.simpleQueryString()
                        .fields("title", "body", "description").matching(pattern))
                .sort(f -> f.field("title_sort").then().field("body_sort").then().field("description_sort"))
                .fetchHits(size.orElse(20));
    }

    /**
     * It searches for blog posts with a title that matches the given title, sorts the results by title, body, and
     * description, and returns the first 20 results
     *
     * @param title The title of the blog post to search for.
     * @param size The number of results to return.
     * @return A list of BlogPost objects
     */
    @GET
    @Path("/search/{title}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public List<BlogPost> searchBlogsByTitle(@RestQuery String title,
                                        @RestQuery Optional<Integer> size) {
        return searchSession.search(BlogPost.class)
                .where(f -> title == null || title.trim().isEmpty() ? f.matchAll()
                        : f.simpleQueryString()
                        .fields("title").matching(title))
                .sort(f -> f.field("title_sort").then().field("body_sort").then().field("description_sort"))
                .fetchHits(size.orElse(20));
    }


    /**
     * It returns a list of all blog posts in the database
     *
     * @return A list of all blog posts
     */
    @GET
    @Path("/getAll")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllBlogPosts() {
        return Response.ok(BlogPost.listAll()).build();
    }


    /**
     * "If the blog post with the given id exists, return it, otherwise return a 404."
     *
     * The @RestQuery annotation is a custom annotation that I created to make it easier to retrieve query parameters. It's
     * a simple annotation that just wraps the JAX-RS @QueryParam annotation
     *
     * @param id The id of the blog post to retrieve
     * @return A Response object.
     */
    @GET
    @Path("/{id}")
    public Response getBlogPostById(@RestQuery Long id) {
        return BlogPost.findByIdOptional(id)
                .map(Response::ok)
                .orElse(Response.status(Response.Status.NOT_FOUND))
                .build();
    }

   /**
     * It returns a blog post by description.
     *
     * @param title The description of the blog post.
     * @return A Response object
     */
     @GET
    @Path("/{title}")
    public Response getBlogPostByTitle(@RestQuery String title) {
        return BlogPost.find("title", title).firstResultOptional()
                .map(Response::ok)
                .orElse(Response.status(Response.Status.NOT_FOUND))
                .build();
    }

    /**
     * It returns a blog post by description.
     *
     * @param description The description of the blog post.
     * @return A Response object
     */
    @GET
    @Path("/{description}")
    public Response getBlogPostByDescription(@RestQuery String description) {
        return BlogPost.find("select description from blog_post ", description).firstResultOptional()
                .map(Response::ok)
                .orElse(Response.status(Response.Status.NOT_FOUND))
                .build();
    }


    /**
     * "If the blog post exists, return it, otherwise return a 404."
     *
     * The first thing we do is call the find method on the BlogPost class. This method is provided by the JPA Entity
     * class. It returns a Query object
     *
     * @param body The name of the parameter.
     * @return A Response object
     */
    @GET
    @Path("/{body}")
    public Response getBlogPostByBody(@RestQuery String body) {
        return BlogPost.find("body", body).firstResultOptional()
                .map(Response::ok)
                .orElse(Response.status(Response.Status.NOT_FOUND))
                .build();
    }


}

package com.guio.models.user;

import com.guio.abstractClasses.AbstractEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "user_user")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Indexed
public class UserUser extends AbstractEntity {


    @Size(max = 128)
    @NotNull
    @Column(name = "password", nullable = false, length = 128)
    private String password;


    @NotNull
    @Column(name = "is_superuser", nullable = false)
    private Boolean isSuperuser = false;

    @Size(max = 254)
    @NotNull
    @Column(name = "email", nullable = false, length = 254)
    private String email;

    @NotNull
    @Column(name = "is_staff", nullable = false)
    private Boolean isStaff = false;

    @NotNull
    @Column(name = "is_active", nullable = false)
    private Boolean isActive = false;

    @Size(max = 255)
    @Column(name = "username")
    private String username;

    @Size(max = 255)
    @NotNull
    @Column(name = "auth_provider", nullable = false)
    private String authProvider;


    @NotNull
    @Column(name = "is_verified", nullable = false)
    private Boolean isVerified = false;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Size(max = 100)
    @NotNull
    @Column(name = "status", nullable = false, length = 100)
    private String status;


    @NotNull
    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserUser userUser = (UserUser) o;
        return id != null && Objects.equals(id, userUser.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
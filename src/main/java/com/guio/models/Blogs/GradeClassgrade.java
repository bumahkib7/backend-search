package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import lombok.Data;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

@Entity
@Table(name = "grade_classgrade")
@Indexed
@Data
public class GradeClassgrade extends AbstractEntity {


    @Size(max = 150)
    @Column(name = "grade", length = 150)
    private String grade;

    @Size(max = 2)
    @NotNull
    @Column(name = "country", nullable = false, length = 2)
    private String country;

    @Size(max = 18)
    @Column(name = "color_code", length = 18)
    private String colorCode;

    @NotNull
    @Column(name = "ranking", nullable = false)
    private Integer ranking;

}
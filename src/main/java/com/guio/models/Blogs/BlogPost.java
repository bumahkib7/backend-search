package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import com.guio.models.user.UserUser;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Type;
import org.hibernate.search.engine.backend.types.Sortable;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "blog_post", schema = "public", indexes = {
        @Index(name = "blog_post_feedback_id_980ddc0a", columnList = "feedback_id"),
        @Index(name = "blog_post_slug_b95473f2_like", columnList = "slug"),
        @Index(name = "blog_post_grade_level_id_2f12c673", columnList = "grade_level_id"),
        @Index(name = "blog_post_owner_id_ff7c9277", columnList = "owner_id")
}, uniqueConstraints = {
        @UniqueConstraint(name = "blog_post_slug_b95473f2_uniq", columnNames = {"slug"})
})
@Indexed(index = "blog_post")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class BlogPost extends AbstractEntity{

    @Size(max = 100)
    @NotNull
    @Column(name = "title", nullable = false, length = 100)
    @FullTextField(analyzer = "title")
    @KeywordField(name = "title_sort",sortable = Sortable.YES, normalizer = "sort")
    private String title;

    @NotNull
    @Column(name = "body", nullable = false)
    @Type(type = "org.hibernate.type.TextType")
    @FullTextField(analyzer = "body")
    @KeywordField(name = "body_sort",sortable = Sortable.YES,normalizer = "sort")
    private String body;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private UserUser owner;

    @Size(max = 400)
    @Column(name = "description", length = 400)
    @FullTextField(analyzer = "description")
    @KeywordField(name = "description_sort", normalizer = "sort", sortable = Sortable.YES)
    private String description;

    @Size(max = 100)
    @NotNull
    @Column(name = "slug", nullable = false, length = 100)
    @KeywordField
    private String slug;

    @Size(max = 900)
    @Column(name = "image", length = 900)
    private String image;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "grade_level_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private GradeClassgrade gradeLevel;

    @Size(max = 100)
    @NotNull
    @Column(name = "status", nullable = false, length = 100)
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "feedback_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private Feedbackmessage feedback;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BlogPost blogPost = (BlogPost) o;
        return id != null && Objects.equals(id, blogPost.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


}



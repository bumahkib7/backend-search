package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import com.guio.models.user.UserUser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexingDependency;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "blog_comment", schema = "public", indexes = {@Index(name = "blog_comment_post_id_580e96ef", columnList = "post_id"), @Index(name = "blog_comment_owner_id_d904784e", columnList = "owner_id")})
@Getter
@Setter
@ToString(callSuper = true)
@RequiredArgsConstructor
@Indexed(index = "blog_comment")

public class Comment extends AbstractEntity {

    @NotNull
    @Column(name = "body", nullable = false)
    @FullTextField(analyzer = "body1")
    private String body;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    @ToString.Exclude
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private UserUser owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id")
    @ToString.Exclude
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private BlogPost post;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Comment that = (Comment) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
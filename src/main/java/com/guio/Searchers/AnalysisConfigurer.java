package com.guio.Searchers;

import io.quarkus.hibernate.search.orm.elasticsearch.SearchExtension;
import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurationContext;
import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurer;
import org.jetbrains.annotations.NotNull;

import javax.inject.Named;

@SearchExtension
@Named("analysisConfigurer")
public class AnalysisConfigurer implements ElasticsearchAnalysisConfigurer {

    /**
     * The function configures the analyzers and normalizers that will be used by the Elasticsearch index
     *
     * @param context The context object that allows you to configure the analyzers, tokenizers, and token filters.
     */
    @Override
    public void configure( @NotNull ElasticsearchAnalysisConfigurationContext context) {
        context.analyzer("title").custom()
                .tokenizer("standard")
                .tokenFilters("lowercase", "asciifolding");

        context.analyzer("body").custom()
                .tokenizer("standard")
                .tokenFilters("lowercase", "asciifolding");

        context.analyzer("description").custom()
                .tokenizer("standard")
                .tokenFilters("lowercase", "asciifolding");

        context.analyzer("body1").custom()
                .tokenizer("standard")
                .tokenFilters("lowercase", "asciifolding");

        context.normalizer("sort").custom()
                .tokenFilters("lowercase", "asciifolding");


    }

}
